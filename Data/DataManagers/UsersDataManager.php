<?php

namespace Data\DataManagers;

use Business\Enums\AccessTokenTypesEnum;
use Business\Models\UserAccessTokenModel;
use Business\Security\Tokens;
use Data\Repositories\PasswordResetLinksRepository;
use Data\Repositories\RolePermissionsRepository;
use Data\Repositories\UserAccessTokensRepository;
use Data\Repositories\UserRolesRepository;
use Data\Repositories\UsersRepository;

class UsersDataManager {

	public static function GetUsers() {
		return UsersRepository::Get();
	}

	public static function GetAdmins() {
		return UsersRepository::GetAdmins();
	}

	public static function GetUser($userId) {
		return UsersRepository::GetOne(["UserId" => $userId]);
	}

	public static function GetUserByUsername($username) {
		return UsersRepository::GetOne(["Username" => $username]);
	}

	public static function InsertUser($model) {
		return UsersRepository::Insert($model);
	}

	public static function UpdateUser($model) {
		return UsersRepository::Update($model);
	}

	public static function DeleteUser($userId) {
		return UsersRepository::Delete($userId);
	}


	// Tokens

	/**
	 * Returns access token for user. Active token is returned if $endDate is omitted. Returns null if no token is found.
	 *
	 * @param int $userId
	 * @param string|null $endDate
	 *
	 * @return UserAccessTokenModel|null
	 */
	public static function GetUserAccessToken($userId, $accessTokenTypeId, $endDate = null) {
		return UserAccessTokensRepository::GetOne([
			UserAccessTokensRepository::COLUMN_USER_ID => $userId,
			UserAccessTokensRepository::COLUMN_ACCESS_TOKEN_TYPE_ID => $accessTokenTypeId,
			UserAccessTokensRepository::COLUMN_END_DATE => $endDate
		]);
	}

	/**
	 * Generates and saves new Access Token for User. Returns false if failed, or new Token if successful.
	 *
	 * @param int $userId
	 * @param int $accessTokenType
	 * @return UserAccessTokenModel|bool
	 */
	public static function CreateUserAccessToken($userId, $accessTokenTypeId) {

		$token = new UserAccessTokenModel();
		$token->Token = Tokens::CreateToken();
		$token->UserId = $userId;
		$token->StartDate = date("Y-m-d H:i:s");
		$token->EndDate = null;
		$token->AccessTokenTypeId = $accessTokenTypeId;

		UserAccessTokensRepository::Insert($token);

		return $token;
	}

	/**
	 * Sets EndDate for User Access token which is currently active.
	 *
	 * @param int $userId
	 * @return bool
	 */
	public static function RemoveUserAccessToken($userId, $accessTokenTypeId) {

		return UserAccessTokensRepository::RemoveToken($userId, $accessTokenTypeId);
	}


	// Users

	/**
	 * @param $userId
	 * @return \Business\Models\UserRoleModel[]
	 */
	public static function GetRoles($userId) {
		return UserRolesRepository::Get(["UserId" => $userId]);
	}

	public static function GetRolePermissions($roleId) {
		return RolePermissionsRepository::GetPermissions($roleId);
	}

	public static function GetUserPermissions($userId) {
		return UsersRepository::GetUserPermissions($userId);
	}

	public static function InsertUserRole($userRole) {
		return UserRolesRepository::Insert($userRole);
	}

	public static function UpdateUserRole($userRole) {
		return UserRolesRepository::Update($userRole);
	}

	public static function GetUserByEmail($email) {
		return UsersRepository::GetOne(["Email" => $email]);
	}

	public static function InsertPasswordResetLink($passwordResetLink) {
		return PasswordResetLinksRepository::Insert($passwordResetLink);
	}

	public static function GetPasswordResetLink($token) {
		return PasswordResetLinksRepository::GetOne(["Token" => $token]);
	}

	public static function UpdatePasswordResetLink($model) {

		return PasswordResetLinksRepository::Update($model);
	}

	public static function GetUserResetPasswordLink($guid) {
		return PasswordResetLinksRepository::GetOne(["ResetLink" => $guid]);
	}

	public static function GetUserResetPasswordLinkByUserId($userId) {
		return PasswordResetLinksRepository::GetOne(["UserId" => $userId]);
	}

	public static function DeleteUserResetPasswordLink($PasswordResetLinkId) {
		return PasswordResetLinksRepository::Delete($PasswordResetLinkId);
	}

	public static function CountUsers() {
		return UsersRepository::Count();
	}

	public static function CountAdmins() {
		return UsersRepository::CountAdmins();
	}

}