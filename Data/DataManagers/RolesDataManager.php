<?php


namespace Data\DataManagers;


use Data\Repositories\RolesRepository;

class RolesDataManager {

    public static function GetRoles(){


        return RolesRepository::Get([]);
    }

    public static function GetOneRole($roleId){


        return RolesRepository::GetOne(["RoleId" =>$roleId]);
    }

    public static function UpdateRole($model){


        return RolesRepository::Update($model);
    }

    public static function InsertRole($model){


        return RolesRepository::Insert($model);
    }

    public static function DeleteRole($roleId){
        return RolesRepository::Delete($roleId);
    }

}