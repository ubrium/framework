<?php


namespace Data\DataManagers;


use Data\Repositories\RolePermissionsRepository;

class RolePermissionsDataManager {

    public static function GetRolePermissions($roleId){
        return RolePermissionsRepository::Get(["RoleId"=>$roleId]);
    }

    public static function AddRolePermission($model){
        return RolePermissionsRepository::Insert($model);
    }

    public static function UpdateRolePermission($model){
        return RolePermissionsRepository::Update($model);
    }

    public static function DeleteRolePermission($id){
        return RolePermissionsRepository::Delete($id);
    }

}