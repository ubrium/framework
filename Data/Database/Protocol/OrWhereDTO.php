<?php


namespace Data\Database\Protocol;


class OrWhereDTO {

	public $Column;
	public $Value = null;
	public $Operator = null;

	function __construct($Column, $Value = null, $Operator = null) {
		$this->Column = $Column;
		$this->Value = $Value;
		$this->Operator = $Operator;
	}


}