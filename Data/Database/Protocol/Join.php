<?php

namespace Data\Database\Protocol;


use Data\Repositories\BaseRepository;

/**
 * Class Join
 * @package Data\Database\Protocol
 * @property BaseRepository $Repository
 * @property string $Column
 * @property string $ReferencedColumn
 */
class Join {

	public $Repository;
	public $Column;
	public $ReferencedColumn;
	public $Children;
	public $Type;
	public $Alias;

	/**
	 * @param BaseRepository $repository
	 * @param $column
	 * @param $referencedColumn
	 * @param Join[] $children
	 * @param string $type
	 * @param string $alias
	 */
	function __construct($repository, $column = null, $referencedColumn = null, $children = [], $type = '', $alias = null) {
		$this->Column = $column;
		$this->Repository = $repository;
		$this->ReferencedColumn = $referencedColumn;
		$this->Children = $children;
		$this->Type = $type;
		$this->Alias = $alias;
	}


}