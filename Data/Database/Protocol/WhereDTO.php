<?php


namespace Data\Database\Protocol;


class WhereDTO {

    public $Column;
    public $Value = null;
    public $Operator = null;

    function __construct($Column, $Value = null, $Operator = null) {
        $this->Column = $Column;
        $this->Value = $Value;
        $this->Operator = $Operator;
    }


}