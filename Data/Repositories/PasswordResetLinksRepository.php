<?php

namespace Data\Repositories;

use Business\Models\PasswordResetLinkModel;

/**
 * Class PasswordResetLinksRepository
 * @package Data\Repositories
 * @method static PasswordResetLinkModel[] Get
 * @method static PasswordResetLinkModel GetOne
 */
class PasswordResetLinksRepository extends BaseRepository {
}