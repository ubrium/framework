<?php
use Business\ApiControllers\UsersApiController;
use Business\Enums\AccessTokenTypesEnum;
use Business\Enums\RolesEnum;
use Business\Models\UserModel;
use Data\DataManagers\UsersDataManager;

class SecurityController extends MVCController {

	public function GetLogin() {
		if (Security::IsLoggedIn() === false) {
			$this->_renderLoginPage();
		} else {
			Router::Redirect(Security::RoleDefaultPageRoute(Security::GetCurrentUser()->RoleId));
		}
	}


	public function PostLogin($username, $password, $rememberMe = false) {
		$user = UsersApiController::CheckLoginAdmin($username, $password);
		if ($user !== null) {
			$this->LoginAdmin($user, $rememberMe);
		} else {
			$this->_renderLoginPage();
		}
	}

	public function GetLogout() {
		UsersApiController::RemoveUserAccessToken(Security::GetCurrentUser()->UserId, AccessTokenTypesEnum::Admin);
		Session::Destroy();
		Router::Redirect(Security::RoleDefaultPageRoute(RolesEnum::Visitor));
	}

	public function PostValidateUsername($username) {
		$user = UsersApiController::GetUserByUsername($username);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}

	public function PostValidateEmail($email) {
		$user = UsersApiController::GetUserByEmail($email);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}


	/**
	 * @param UserModel|null $user
	 * @param boolean $rememberMe
	 * @param null|integer $timeout
	 */
	private function LoginAdmin($user, $rememberMe, $timeout = null) {
		$admin = false;

		$userRoles = UsersDataManager::GetRoles($user->UserId);
		foreach ($userRoles as $userRole) {
			if ($userRole->RoleId == RolesEnum::Admin) {
				$admin = true;
			}
		}

		if ($admin == true) {
			if ($oldToken = UsersApiController::GetActiveToken($user->UserId, AccessTokenTypesEnum::Admin) !== null) {
				UsersApiController::RemoveUserAccessToken($user->UserId, AccessTokenTypesEnum::Admin);
			}
			if ($rememberMe !== false) {
				$newToken = UsersApiController::CreateUserAccessToken($user->UserId, AccessTokenTypesEnum::Admin);
				Security::SetUserAccessToken($newToken->Token);
			}

			Security::SetCurrentUser($user, UsersApiController::GetUserPermissions($user->UserId));

			$route = Router::Create(Security::RoleDefaultPageRoute(Security::GetCurrentUser()->RoleId), [], false);
			if(Session::Get("Route") !== false){
				$route = Session::Get("Route");
			}
			if (!is_null($timeout)) {
				$route = sprintf("refresh:5;url=%s", Router::Create($route, [], false));
				header($route);
			} else {
				Router::Redirect($route);
			}
		} else {
			$this->_renderLoginPage();
		}
	}

	private function _renderLoginPage() {
		$this->RenderView("Home/Login");
	}

}