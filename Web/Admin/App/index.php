<?php

try {
	include('../../../GlobalConfig.php');
	include('../../../GlobalAutoload.php');

	include('Components/Autoload.php');

	if(Config::debugMode === false) {
		error_reporting(0);
		ini_set('display_errors', 0);
	} else {
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
	}

	Session::Start();

	$router = new Router();
	if (Config::maintenanceMode && "Maintenance" != $router->Controller) {
		Router::Redirect("maintenance");
	}

	Security::CheckLogin();

	$router->RenderPage();
} catch (MVCException $e) {
	$e->DisplayError();
} catch (Exception $e) {
	var_dump($e);
	die();
}
