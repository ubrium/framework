<?php
use Business\ApiControllers\UsersApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\AccessTokenTypesEnum;
use Business\Enums\RolesEnum;
use Business\Helper\NotificationHelper;
use Business\Models\PasswordResetLinkModel;
use Business\Models\UserModel;
use Business\Security\Crypt;

class SecurityController extends MVCController {

	public function GetLogin() {
		if (Security::IsLoggedIn() === false) {
			$this->_renderLoginPage();
		} else {
			Router::Redirect(Security::RoleDefaultPageRoute(Security::GetCurrentUser()->RoleId));
		}
	}

	public function PostLogin($email, $password, $rememberMe = false) {
		$user = UsersApiController::CheckLogin($email, $password);
		if ($user !== null) {
			$this->LoginUser($user, $rememberMe);
		} else {
			$this->_renderLoginPage();
		}
	}

	public function GetLogout() {
		UsersApiController::RemoveUserAccessToken(Security::GetCurrentUser()->UserId, AccessTokenTypesEnum::Portal);
		Session::Destroy();
		Router::Redirect(Security::RoleDefaultPageRoute(RolesEnum::Visitor));
	}

	public function PostSendEmailToChangePassword($email) {
		$user = UsersApiController::GetUserByEmail($email);

		if (!empty($user)) {
			$oldResetPasswordLink = UsersApiController::GetUserResetPasswordLinkByUserId($user->UserId);
			UsersApiController::DeleteUserResetPasswordLink($oldResetPasswordLink->PasswordResetLinkId);

			$resetPasswordLinkModel = new PasswordResetLinkModel();

			$resetPasswordLinkModel->ResetLink = CommonHelper::GenerateGUID();
			$resetPasswordLinkModel->UserId = $user->UserId;

			UsersApiController::InsertPasswordResetLink($resetPasswordLinkModel);

			$mailModel = new EmailToResetPasswordViewModel();

			$mailModel->Name = $user->Name;
			$mailModel->PasswordResetLink = $resetPasswordLinkModel->ResetLink;

			NotificationHelper::Send(
				"Reset password email",
				$this->RenderView("Mail/ResetPasswordEmail", ["model" => $mailModel], false),
				"",
				[new SenderDTO("gynny", "support@gynny.de")],
				[new RecipientDTO($user->Name, $user->Email)]
			);

			Router::Redirect("reset-password-email-successfully-sent");
		} else {
			Router::Redirect("reset-password-email-unsuccessfully-sent");
		}
	}

	public function GetResetPassword($guid) {
		$user = UsersApiController::GetUserResetPasswordLink($guid);

		if (!empty($user)) {
			$this->RenderView("Registration/ResetPassword", ["model" => $user]);
		} else {
			Router::Redirect("incorrect-link");
		}
	}

	public function PostResetPassword($password, $confirmPassword, $userId) {
		if ($password == $confirmPassword) {
			$user = UsersApiController::GetUser($userId);
			$user->Password = Crypt::HashPassword($confirmPassword);
			UsersApiController::UpdateUser($user);

			Router::Redirect("reset-password-successful");
		} else {
			Router::Redirect("reset-password-unsuccessful");
		}
	}

	public function PostValidateUsername($username) {
		$user = UsersApiController::GetUserByUsername($username);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}

	public function PostValidateEmail($email) {
		$user = UsersApiController::GetUserByEmail($email);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}


	/**
	 * @param UserModel|null $user
	 * @param boolean $rememberMe
	 * @param null|integer $timeout
	 */
	public function LoginUser($user, $rememberMe, $timeout = null) {
		if ($user->ConfirmRegistration != 1) {
			$this->RenderView("Registration/EmailNotConfirmedMessage", ["model" => $user]);
		}
		if ($user->Active == 1) {
			if ($oldToken = UsersApiController::GetActiveToken($user->UserId, AccessTokenTypesEnum::Portal) !== null) {
				UsersApiController::RemoveUserAccessToken($user->UserId, AccessTokenTypesEnum::Portal);
			}
			if ($rememberMe !== false) {
				$newToken = UsersApiController::CreateUserAccessToken($user->UserId, AccessTokenTypesEnum::Portal);
				Security::SetUserAccessToken($newToken->Token);
			}

			Security::SetCurrentUser($user, UsersApiController::GetUserPermissions($user->UserId));

			$route = Router::Create(Security::RoleDefaultPageRoute(Security::GetCurrentUser()->RoleId), [], false);
			if(Session::Get("Route") !== false){
				$route = Session::Get("Route");
			}
			if (!is_null($timeout)) {
				$route = sprintf("refresh:5;url=%s", Router::Create($route, [], false));
				header($route);
			} else {
				Router::Redirect($route);
			}
		} else {
			$this->_renderLoginPage();
		}
	}

	private function _renderLoginPage() {
		$this->RenderView("Home/Login");
	}

} 