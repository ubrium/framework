<?php

use Business\ApiControllers\UsersApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\RolesEnum;
use Business\Helper\NotificationHelper;
use Business\Models\ConfirmationLinkModel;
use Business\Models\UserModel;
use Business\Models\UserRoleModel;
use Business\Security\Crypt;
use Data\DataManagers\UsersDataManager;
use Data\Repositories\ConfirmationLinksRepository;

class UsersController extends MVCController {

	public function GetRegistration() {
		$this->RenderView("Registration/Registration");
	}

	public function PostRegistration($name, $email, $username, $password, $confirmPassword, $image = null) {

		$userInSystem = UsersApiController::GetUserByEmail($email);

		if (empty($userInSystem) && $password === $confirmPassword) {
			$newImageName = null;
			if ($image['name'] !== "") {
				$newImageName = self::_generateImageName($image['name'], $name);
				move_uploaded_file($image['tmp_name'], self::_generateImageFullPath($newImageName));
			}

			$newUser = UsersApiController::Register($name, $email, $username, $password, $newImageName, RolesEnum::User);

			//sending email with confirmation link
			$confirmationLink = self::_createConfirmationLink($newUser->UserId);

			$mailModel = new UserRegisteredViewModel();

			$mailModel->Name = $name;
			$mailModel->ConfirmationLink = $confirmationLink->ConfirmationLink;

			NotificationHelper::Send(
				"Welcome",
				$this->RenderView("Mail/ConfirmationMail", ["model" => $mailModel], false),
				"Account Confirmation",
				[new SenderDTO("ubrium", "confirmation@ubrium.com")],
				[new RecipientDTO($name, $email)]
			);

			Router::Redirect("confirmation-link-sent");
		} else {
			Router::Redirect("registration");
		}
	}

	public function GetSendConfirmationEmail($userId) {
		$confirmationLink = self::_createConfirmationLink($userId);
		$userModel = UsersApiController::GetUser($userId);

		//sending email with confirmation link
		$mailModel = new UserRegisteredViewModel();

		$mailModel->Name = $userModel->Name;
		$mailModel->ConfirmationLink = $confirmationLink->ConfirmationLink;

		NotificationHelper::Send(
			"Welcome",
			$this->RenderView("Mail/ConfirmationMail", ["model" => $mailModel], false),
			"Account Confirmation",
			[new SenderDTO("ubrium", "confirmation@ubrium.com")],
			[new RecipientDTO($userModel->Name, $userModel->Email)]
		);

		Router::Redirect("confirmation-link-sent");
	}

	public function GetActivateUser($guid) {
		$confirmationLink = UsersApiController::GetConfirmationLinkByGuid($guid);

		$now = new DateTime();
		if (!empty($confirmationLink)) {
			if ($now <= DateTime::createFromFormat("Y-m-d H:i:s", $confirmationLink->ExpirationDate)) {
				UsersApiController::ActivateUser($confirmationLink->UserId);

				$user = UsersApiController::GetUser($confirmationLink->UserId);

				Security::SetCurrentUser($user, UsersApiController::GetUserPermissions($user->UserId));

				Router::Redirect("successful-registration");

			} else {
				Router::Redirect("unsuccessful-registration");
			}
		} else {
			Router::Redirect("home");
		}

	}

	public function PostComparePasswordAndEmail($email, $password) {

		$user = UsersApiController::GetUserByEmail($email);

		if (Crypt::CheckPassword($password, $user->Password)) {
			$userExists = true;
		} else {
			$userExists = false;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}

	public function PostValidateEmail($email) {
		$user = UsersApiController::GetUserByEmail($email);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}

	public function PostValidatePasswordWhenUserChange($password) {
		$currentUser = Security::GetCurrentUser();

		$user = UsersApiController::GetUser($currentUser->UserId);

		if ($user === null) {
			$passwordState = false;
		} else {
			if (Crypt::CheckPassword($password, $user->Password)) {
				$passwordState = true;
			} else {
				$passwordState = false;
			}
		}

		echo json_encode(
			(object)[
				"Success" => $passwordState
			]
		);
	}

	public function GetDeleteUser() {
		$success = UsersApiController::DeleteUser(Security::GetCurrentUser()->UserId);

		Router::Redirect("logout");
	}

	public function GetConfirmationLinkSent() {
		$this->RenderView("Registration/ConfirmationEmailMessage");
	}

	public function GetSuccessfulRegistration() {
		$this->RenderView("Registration/SuccessfulRegistration");
	}

	public function GetUnsuccessfulRegistration() {
		$this->RenderView("Registration/UnsuccessfulRegistration");
	}

	public function GetSuccessfulPasswordReset() {
		$this->RenderView("Registration/SuccessfulPasswordReset");
	}

	public function GetUnSuccessfulPasswordReset() {
		$this->RenderView("Registration/UnSuccessfulPasswordReset");
	}

	public function GetIncorrectResetPasswordLink() {
		$this->RenderView("Registration/IncorrectLink");
	}

	public function GetResetPasswordSuccessful() {
		$this->RenderView("Registration/ResetPasswordSuccessful");
	}

	public function GetResetPasswordUnSuccessful() {
		$this->RenderView("Registration/ResetPasswordUnsuccessful");
	}

	public function GetEditProfile() {
		$model = new UserViewModel();
		$model->User = UsersApiController::GetUser(Security::GetCurrentUser()->UserId);

		$this->RenderView("Users/EditProfile", ["model" => $model]);
	}

	public function PostEditProfile($name, $username, $image = null) {
		$currentUser = Security::GetCurrentUser();
		$user = UsersApiController::GetUser($currentUser->UserId);

		$user->Name = $name;
		$user->Username = $username;

		if ($image['name'] !== "") {
			$newName = self::_generateImageName($image['name'], $name);
			move_uploaded_file($image['tmp_name'], self::_generateImageFullPath($newName));
			$user->Image = $newName;
		}

		UsersApiController::UpdateUser($user);

		Security::SetCurrentUser($user, $currentUser->Permissions);

		Router::Redirect("edit-profile");
	}

	public function PostChangePassword($currentPassword, $newPassword, $repeatPassword) {
		$currentUser = Security::GetCurrentUser();
		$user = UsersApiController::GetUser($currentUser->UserId);

		if (Crypt::CheckPassword($currentPassword, $user->Password)) {
			if ($newPassword == $repeatPassword) {
				$user->Password = Crypt::HashPassword($repeatPassword);

				UsersApiController::UpdateUser($user);
			}
		}

		Router::Redirect("edit-profile");
	}

	public function PostChangeEmail($currentPassword, $newEmail, $repeatEmail) {
		$currentUser = Security::GetCurrentUser();
		$user = UsersApiController::GetUser($currentUser->UserId);

		if (Crypt::CheckPassword($currentPassword, $user->Password)) {
			if ($newEmail == $repeatEmail) {
				$user->Email = $repeatEmail;

				UsersApiController::UpdateUser($user);

				$currentUser->Email = $newEmail;
				Security::SetCurrentUser($user, $currentUser->Permissions);
			}
		}

		Router::Redirect("edit-profile");
	}

	/**
	 * @param $userId
	 * @return ConfirmationLinkModel
	 */
	private function _createConfirmationLink($userId) {
		$confirmationLink = new ConfirmationLinkModel();
		$confirmationLink->UserId = $userId;
		$confirmationLink->ConfirmationLink = CommonHelper::GenerateGUID();

		ConfirmationLinksRepository::Insert($confirmationLink);
		return $confirmationLink;
	}

	private static function _generateImageName($image, $name) {
		return CommonHelper::StringToFilename(sprintf("user-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($image)));
	}

	private static function _generateImageFullPath($imageName) {
		return sprintf("%s/Media/Users/%s", CDN_PATH, $imageName);
	}

}