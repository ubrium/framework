<?php


class ErrorController extends MVCController {
	public function PageNotFound() {
		$this->RenderView("Error/404");
	}

	public function AccessDenied() {
		$this->RenderView("Error/401");
	}

	public function GeneralError() {
		$this->RenderView("Error/Error");
	}

}