<?php

/**
 * Class CurrentUserDTO
 *
 */
class CurrentUserDTO {

    public $Username;
    public $Name;
    public $Email;
    public $UserId;
    public $Image;

    public $RoleId;
    public $Permissions = [];

} 