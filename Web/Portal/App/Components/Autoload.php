<?php

define("PATH", dirname(dirname(__FILE__)));

function MVCAutoloader($class_name)
{
    if (preg_match('/Enum$/', $class_name)) {
        $path = PATH . '/Enum/' . $class_name . '.php';
    } elseif (preg_match('/Helper$/', $class_name)) {
        $path = PATH . '/Helper/' . $class_name . '.php';
    } elseif (preg_match('/DTO$/', $class_name)) {
        $path = PATH . '/DTO/' . $class_name . '.php';
    } elseif (preg_match('/Controller$/', $class_name)) {
        $path = PATH . '/Controller/' . $class_name . '.php';
    } elseif (preg_match('/Model$/', $class_name)) {
        $path = PATH . '/Model/' . $class_name . '.php';
    } elseif (preg_match('/Repository$/', $class_name)) {
        $path = PATH . '/Repository/' . $class_name . '.php';
    } elseif (preg_match('/Config$/', $class_name)) {
        $path = PATH . '/Config/' . $class_name . '.php';
    } elseif (preg_match('/Exception$/', $class_name)) {
        $path = PATH . '/Exception/' . $class_name . '.php';
    } else {
        $path = PATH . '/Class/' . $class_name . '.php';
    }


    if (is_readable($path)) {
        require_once($path);
    }
}

spl_autoload_register("MVCAutoloader");