<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			"example" => new RouteDTO("example/{Id:integer}/{Slug:string}", "Home", "Example"),

			// Home
			"home" => new RouteDTO("home", "Home", "Home"),
			"index" => new RouteDTO("", "Home", "Home"),

			// Users
			"login" => new RouteDTO("login", "Security", "Login"),
			"registration" => new RouteDTO("registration", "Users", "Registration"),
			"new-user-confirmation-link" => new RouteDTO("new-user-confirmation-link/{guid:string}", "Users", "ActivateUser"),
			"confirmation-link-sent" => new RouteDTO("confirmation-link", "Users", "ConfirmationLinkSent"),
			"successful-registration" => new RouteDTO("successful-register", "Users", "SuccessfulRegistration"),
			"unsuccessful-registration" => new RouteDTO("unsuccessful-register", "Users", "UnsuccessfulRegistration"),
			"compare-password-and-email" => new RouteDTO("compare-password-and-email", "Users", "ComparePasswordAndEmail"),
			"validate-email" => new RouteDTO("validation-email", "Users", "ValidateEmail"),
			"validate-password-user-change" => new RouteDTO("validation-password-user-change", "Users", "ValidatePasswordWhenUserChange"),
			"logout" => new RouteDTO("logout", "Security", "Logout"),
			"reset-user-password" => new RouteDTO("reset-password/{guid:string}", "Security", "ResetPassword"),
			"reset-user-password-form" => new RouteDTO("reset-password-form", "Security", "ResetPassword"),
			"send-email-to-change-password" => new RouteDTO("send-email-to-change-password", "Security", "SendEmailToChangePassword"),
			"reset-password-email-successfully-sent" => new RouteDTO("reset-password-email-successfully-sent", "Users", "SuccessfulPasswordReset"),
			"reset-password-email-unsuccessfully-sent" => new RouteDTO("reset-password-email-unsuccessfully-sent", "Users", "UnSuccessfulPasswordReset"),
			"incorrect-link" => new RouteDTO("incorrect-link", "Users", "IncorrectRessetPasswordLink"),
			"reset-password-successful" => new RouteDTO("reset-password-successful", "Users", "ResetPasswordSuccessful"),
			"reset-password-unsuccessful" => new RouteDTO("reset-password-unsuccessful", "Users", "ResetPasswordUnSuccessful"),
			"edit-profile" => new RouteDTO("edit-profile", "Users", "EditProfile"),
			"change-password" => new RouteDTO("change-password", "Users", "ChangePassword"),
			"change-email" => new RouteDTO("change-email", "Users", "ChangeEmail"),
			"delete-user" => new RouteDTO("delete-user", "Users", "DeleteUser"),
			"deactivate-user" => new RouteDTO("deactivate-user", "Users", "DeactivateUser"),
			"send-confirmation-email" => new RouteDTO("send-confirmation-email/{userId:integer}", "Users", "SendConfirmationEmail"),

			// Maintenance
			"maintenance"=> new RouteDTO("maintenance", "Maintenance", "Index"),
		);
		return $routes;
	}

}