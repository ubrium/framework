<?php


abstract class MVCException extends Exception {

    public abstract function DisplayError();

} 