<?php

class PageNotFoundException extends MVCException {

    public function DisplayError() {

        $controller = new ErrorController(false);
        $controller->PageNotFound();

    }
}