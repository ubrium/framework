<?php

class HtmlHelper {

	public static function ActiveButton($controller, $action = false, $class = "active") {
		global $router;
		if ($router->Controller === $controller && (!$action || $action === $router->Action)) {
			return $class;
		}
		return "";
	}

}