<?php


namespace Business\Enums;


class PermissionsEnum extends BaseEnum
{

	// Admin permissions
	const Dashboard = 1;
	const ViewUsers = 2;
	const AddUsers = 3;
	const EditUsers = 4;
	const DeleteUsers = 5;

	// User permissions
	const Profile = 20;

}