<?php

namespace Business\Enums;


class LanguageCodesEnum extends BaseEnum {

    const en = 1;
    const de = 2;

}