<?php

namespace Business\Enums;


use ReflectionClass;

abstract class BaseEnum
{

    public $descriptions = array();

    public static function enum($useDescriptions = true)
    {
        $reflect = new ReflectionClass( get_called_class() );
        $constants = $reflect->getConstants();
		$enum = $reflect->newInstance();
		if($useDescriptions === true && isset($enum->Descriptions) && count($enum->Descriptions) > 0) {
			$descriptions = [];
			foreach ($constants as $constant => $value){
				if(isset($enum->Descriptions[$value])) {
					$descriptions[$enum->Descriptions[$value]] = $value;
				} else {
					$descriptions[$constant] = $value;
				}
			}
			return $descriptions;
		} else {
			return $constants;
		}
	}
	
	public function Caption($id) {
		$enum = $this->enum();
		foreach ($enum as $key => $value) {
			if ($value == $id) return $key;
		}
		return false;
	}

    public static function GetConstant($constant) {
        $class = get_called_class();
        $constant = str_replace(" ", "", $constant);
        $reflectionClass = new ReflectionClass($class);
        if($reflectionClass->hasConstant($constant)) {
            return $reflectionClass->getConstant($constant);
        }
        else {
            return null;
        }
    }

    public static function Description($id)
    {
        $class = get_called_class();
        $reflectionClass = new ReflectionClass($class);
        $enum = $reflectionClass->newInstance();
        if(isset($enum->Descriptions[$id])) {
            return $enum->Descriptions[$id];
        }
        return $enum->Caption($id);
    }



}
?>