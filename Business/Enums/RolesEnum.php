<?php

namespace Business\Enums;

class RolesEnum extends BaseEnum {


    const Admin = 1;
    const User = 2;
    const Visitor = 3;

    public $Descriptions = [];

    public function __construct() {
        $this->Descriptions = [
            1 => "Admin",
            2 => "User",
            3 => "Visitor"

        ];
    }
}
