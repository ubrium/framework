<?php


namespace Business\Models;

/**
 * Class RolePermissionModel
 * @package Business\Models
 * @property integer $RolePermissionId;
 * @property integer $PermissionId
 * @property integer $RoleId
 */
class RolePermissionModel {

    public $RolePermissionId;
    public $RoleId;
    public $PermissionId;
    public $Protected;

} 