<?php

namespace Business\Models;

/**
 * Class UserRoleModel
 * @package Business\Models
 * @property integer $UserId
 * @property integer $UserRoleId
 * @property integer $RoleId
 */
class UserRoleModel
{

    public $UserRoleId;
    public $UserId;
    public $RoleId;
}