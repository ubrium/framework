<?php

namespace Business\Security;


class Tokens {

    public static function CreateToken() {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }

}