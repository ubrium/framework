<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 05-Aug-15
 * Time: 13:35
 */

namespace Business\DTO;


class RecipientDTO {

    public $Name;
    public $Address;

    /**
     * RecipientDTO constructor.
     * @param $address
     * @param $name
     */
    public function __construct($name = null, $address = null) {
        $this->Address = $address;
        $this->Name = $name;
    }


}